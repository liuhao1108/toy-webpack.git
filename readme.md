## 文件说明
```markdown
toy-webpack
|-dist	// 输出目录
	|-main.js
|-lib
	|-webpack.js	// 自定义webpack类
|-src	// 源码目录
	|-index.js
	|-expo.js
|-bundle.js	// 启动器
|-webpack.config.js	// 配置文件
```
1. git clone
2. npm install

编译方式—：`node .\bundle.js`